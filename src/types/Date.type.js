import { GraphQLScalarType } from 'graphql';
import Moment from 'moment';

export default new GraphQLScalarType({
  name: 'Date',
  description: 'Formata a data para ISOString',
  serialize(value) {
    return Moment(value).isValid() ? Moment.parseZone(value).format() : null;
  },
  parseValue(value) {
    return Moment(value).isValid() ? Moment.parseZone(value).format() : null;
  },
  parseLiteral(value) {
    return Moment(value).isValid() ? Moment.parseZone(value).format() : null;
  },
});
