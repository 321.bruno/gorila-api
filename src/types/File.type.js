import {
  GraphQLObjectType,
  GraphQLString,
} from 'graphql';

export default new GraphQLObjectType({
  name: 'File',
  fields: () => ({
    filename: {
      type: GraphQLString,
      description: 'File name',
    },
    mimetype: {
      type: GraphQLString,
      description: 'Mime type of file',
    },
    encoding: {
      type: GraphQLString,
      description: 'Encoding of file',
    },
  }),
});
