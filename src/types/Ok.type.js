import {
  GraphQLObjectType,
  GraphQLBoolean,
} from 'graphql';

export default new GraphQLObjectType({
  name: 'Ok',
  fields: () => ({
    ok: {
      type: GraphQLBoolean,
      description: 'Ok',
    },
  }),
});
