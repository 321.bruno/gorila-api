import DateType from './Date.type';

describe('Type Date', () => {
  it('check name and description', () => {
    expect(DateType.name).toEqual('Date');
    expect(DateType.description).toEqual('Formata a data para ISOString');
  });

  it('check functions', () => {
    expect(DateType._scalarConfig.serialize).toBeDefined();
    expect(DateType._scalarConfig.parseValue).toBeDefined();
    expect(DateType._scalarConfig.parseLiteral).toBeDefined();
  });
});
