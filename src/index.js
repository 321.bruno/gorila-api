import { ApolloServer } from 'apollo-server';
import mongoose from 'mongoose';
import config from './config';
import schema from './schema';

class Api {
  constructor() {
    this.connectionMongo();
    return this.startGraphql();
  }

  connectionMongo() {
    const {
      host,
      port,
      database,
    } = config.database;

    const uri = process.env.MONGODB_URI || `mongodb://${host}:${port}/${database}`;

    mongoose.Promise = Promise;

    mongoose.connect(process.env.MONGODB_URI || uri, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
    });

    mongoose.connection
      .on('error', err => console.error(`MongoDB connection error: ${err}`)); //eslint-disable-line

    mongoose.connection
      .on('connected', () => console.info(`MongoDB connection open to: ${uri}`)); //eslint-disable-line

    mongoose.connection
      .on('disconnected', () => console.error('MongoDB connection disconnected')); //eslint-disable-line

    return this;
  }

  // eslint-disable-next-line class-methods-use-this
  startGraphql() {
    const server = new ApolloServer({
      schema,
      onHealthCheck: () => Promise.resolve(),
    });

    return server.listen(config.port, () => {
      // TODO: add log management
      console.log(`🚀  Server ready at http://localhost:${config.port}`); //eslint-disable-line
    });
  }
}

export default new Api();
