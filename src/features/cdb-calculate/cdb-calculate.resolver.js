import moment from 'moment';
import business from 'moment-business';
import csvtojson from 'csvtojson';
import CdiPricesDatabaseModel from './cdi-prices.database.model';

export default class CdbCalculateResolver {
  static cdbCalculate(args) {
    const {
      investmentDate,
      cdbRate,
      baseValue,
      currentDate,
    } = args;

    return CdiPricesDatabaseModel.getAll({
      dtDate: {
        $gte: investmentDate,
        $lte: currentDate,
      },
    })
      .then((cdiPrices) => {
        const history = this._calcAccumulated(cdiPrices, cdbRate, baseValue, investmentDate);
        const [
          item,
        ] = history || [];

        const {
          unitPrice = 0,
          factor = 0,
        } = item || {};

        return {
          finalValue: unitPrice,
          cdiRate: ((unitPrice - baseValue) / baseValue) * 100,
          factor,
          history,
        };
      });
  }

  static _calcAccumulated(cdiPrices, cdbRate, baseValue, investmentDate) {
    return cdiPrices.map((item) => {
      const {
        dLastTradePrice: cdiPrice,
        dtDate: date,
      } = item;

      const bussinesDays = business.weekDays(
        moment.utc(moment(investmentDate).format('YYYY-MM-DD')),
        moment.utc(date),
      );
      const cdiRateAccumulated = this._calcCdiRateAccumulated(
        cdiPrice,
        cdbRate,
        bussinesDays,
      );
      const unitPrice = Math.floor((baseValue * cdiRateAccumulated) * 100) / 100;

      return {
        date,
        factor: cdiRateAccumulated,
        unitPrice,
      };
    });
  }

  static _calcCdiRateAccumulated(cdiPrice, cdbRate, bussinesDays) {
    const cdiRateCalc = ((cdiPrice / 100) + 1) ** (1 / 252);
    const cdiRate = parseFloat((cdiRateCalc - 1).toFixed(8));
    const cdiRateAccumulated = (1 + cdiRate * (cdbRate / 100));
    return (cdiRateAccumulated ** bussinesDays).toFixed(8);
  }

  static uploadMarketData() {
    return CdiPricesDatabaseModel.clean()
      .then(() => csvtojson()
        .fromFile('./cdi_prices.csv')
        .then((items) => Promise.all(items.map((item) => CdiPricesDatabaseModel.create({
          ...item,
          dtDate: moment.utc(item.dtDate, 'DD/MM/YYYY').toDate(),
          dLastTradePrice: parseFloat(item.dLastTradePrice),
        }))))
        .then(() => ({
          filename: 'cdi_prices.csv',
          mimetype: 'text/csv',
          encoding: 'utf-8',
        })));
  }
}
