import {
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLFloat,
} from 'graphql';
import DateType from '../../types/Date.type';

export default new GraphQLObjectType({
  name: 'CdbCalculateHistory',
  description: 'Histórico dos CDbs calculados',
  fields: () => ({
    date: {
      type: new GraphQLNonNull(DateType),
      description: 'Data da taxa',
    },
    unitPrice: {
      type: new GraphQLNonNull(GraphQLFloat),
      description: 'Preço unitário do CDB',
    },
  }),
});
