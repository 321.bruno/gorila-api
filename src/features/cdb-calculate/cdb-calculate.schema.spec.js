import {
  GraphQLNonNull,
  GraphQLFloat,
} from 'graphql';

import CdbCalculateSchema from './cdb-calculate.schema';
import DateType from '../../types/Date.type';

describe('Schema CdbCalculateSchema', () => {
  it('check name and description', () => {
    expect(CdbCalculateSchema.name).toEqual('CdbCalculate');
    expect(CdbCalculateSchema.description).toEqual('Calcula o CDB pós fixado indexado ao CDI');
  });

  it('check fields', () => {
    expect(CdbCalculateSchema._typeConfig.fields()).toEqual({
      date: {
        type: new GraphQLNonNull(DateType),
        description: 'Data da taxa',
      },
      unitPrice: {
        type: new GraphQLNonNull(GraphQLFloat),
        description: 'Preço unitário do CDB',
      },
    });
  });
});
