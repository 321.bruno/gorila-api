import { GraphQLUpload } from 'graphql-upload';
import FileType from '../../types/File.type';
import CdbCalculateResolver from './cdb-calculate.resolver';

export default {
  uploadMarketData: {
    type: FileType,
    description: 'Calcula o CDB pós fixado indexado ao CDI',
    args: {
      file: {
        type: GraphQLUpload,
        description: 'Arquivo CSV dos preços de CDI',
      },
    },
    resolve: (parent, args) => CdbCalculateResolver.uploadMarketData(args),
  },
};
