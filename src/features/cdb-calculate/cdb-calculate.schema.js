import {
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLFloat,
  GraphQLList,
} from 'graphql';
import CdbCalculateHistorySchema from './cdb-calculate-history.schema';

export default new GraphQLObjectType({
  name: 'CdbCalculate',
  description: 'Calcula o CDB pós fixado indexado ao CDI',
  fields: () => ({
    factor: {
      type: new GraphQLNonNull(GraphQLFloat),
      description: 'Fator acumulado',
    },
    cdiRate: {
      type: new GraphQLNonNull(GraphQLFloat),
      description: 'Taxa',
    },
    finalValue: {
      type: new GraphQLNonNull(GraphQLFloat),
      description: 'Valor calculado',
    },
    history: {
      type: new GraphQLNonNull(new GraphQLList(CdbCalculateHistorySchema)),
      description: 'Histórico',
    },
  }),
});
