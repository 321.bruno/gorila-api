import {
  GraphQLNonNull,
  GraphQLFloat,
} from 'graphql';
import DateType from '../../types/Date.type';
import CdbCalculateSchema from './cdb-calculate.schema';
import CdbCalculateResolver from './cdb-calculate.resolver';

export default {
  cdbCalculate: {
    type: CdbCalculateSchema,
    description: 'Calcula o CDB pós fixado indexado ao CDI',
    args: {
      investmentDate: {
        type: new GraphQLNonNull(DateType),
        description: 'Data inicial do investimento',
      },
      cdbRate: {
        type: new GraphQLNonNull(GraphQLFloat),
        description: 'Taxa do CDB',
      },
      baseValue: {
        type: new GraphQLNonNull(GraphQLFloat),
        description: 'Valor base',
      },
      currentDate: {
        type: new GraphQLNonNull(DateType),
        description: 'Data atual',
      },
    },
    resolve: (parent, args) => CdbCalculateResolver.cdbCalculate(args),
  },
};
