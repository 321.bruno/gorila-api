import CdiPricesDatabaseSchema from './cdi-prices.database.schema';

export default class CdiPricesDatabaseModel {
  static getAll(where = {}) {
    return CdiPricesDatabaseSchema
      .find(where)
      .lean();
  }

  static create(price) {
    return CdiPricesDatabaseSchema.create(price);
  }

  static clean() {
    return CdiPricesDatabaseSchema.deleteMany({});
  }
}
