import mongoose from 'mongoose';

class CdiPricesDatabaseSchema {
  constructor() {
    return mongoose.model('CdiPrices', new mongoose.Schema({
      sSecurityName: {
        type: String,
      },
      dtDate: {
        type: Date,
      },
      dLastTradePrice: {
        type: Number,
      },
    }));
  }
}

export default new CdiPricesDatabaseSchema();
