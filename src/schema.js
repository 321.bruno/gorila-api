import {
  GraphQLObjectType,
  GraphQLSchema,
} from 'graphql';
import * as listQueries from './features/**/*.queries.js'; //eslint-disable-line
import * as listMutations from './features/**/*.mutations.js'; //eslint-disable-line

const formatImport = (schema) => Object.keys(schema)
  .map((item) => schema[item])
  .reduce((itemA, itemB) => ({ ...itemA, ...itemB }), {});

export default new GraphQLSchema({
  query: new GraphQLObjectType({
    name: 'Query',
    fields: () => ({
      ...formatImport(listQueries),
    }),
  }),
  mutation: new GraphQLObjectType({
    name: 'Mutations',
    fields: () => ({
      ...formatImport(listMutations),
    }),
  }),
});
