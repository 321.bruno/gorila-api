export default {
  port: process.env.PORT || 9000,
  database: {
    host: 'localhost',
    port: 27017,
    database: 'gorila',
  },
};
