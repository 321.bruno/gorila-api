# Gorila API
https://gorila-api-graphql.herokuapp.com/

Api faz o cálculo baseado no histórico do CDI.

## Stack
* NodeJS (Apollo Server GraphQL)
* Mongoose
* PM2

### Requisitos
 * Node 10.x
 * Yarn 1.19.x

### Instalação

```shell script
yarn
```

### Desenvolvimento

```shell script
yarn server
```

### Testes

```shell script
yarn test
```
### Lint

```shell script
yarn lint
```

### Produção

```shell script
yarn server
```

### Request para popular o banco com o CSV
```shell script
curl -i -H 'Content-Type: application/json' -X POST -d '{"query": "mutation {uploadMarketData{ filename }}"}' https://gorila-api-graphql.herokuapp.com/
```

### CI/CD e Hospedagem

* Gitlab (.gitlab-ci.yml)
* Heroku (Procfile)

### TODOs

* Melhorar deploy CDI Prices
* Finalizar os testes unitários
