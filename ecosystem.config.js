module.exports = {
  apps : [{
    name: 'API',
    script: 'index.js',
    ignore_watch: ['node_modules', 'src'],
    max_restarts: 5,
    min_uptime: 5000,
    env: {
      watch: true,
      ENV: 'development',
      NODE_ENV: 'development',
    },
    env_production : {
      watch: false,
      ENV: 'production',
      NODE_ENV: 'production',
    }
  }],
};
