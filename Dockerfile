FROM node:10-alpine

MAINTAINER Bruno Oliveira <321.bruno@gmail.com>

WORKDIR /app

COPY dist/index.js /app
COPY ecosystem.config.js /app
COPY package.json /app
COPY yarn.lock /app
COPY languages /app/languages

RUN yarn --production

EXPOSE 9000

CMD yarn start:prod
