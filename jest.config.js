module.exports = {
  verbose: true,
  testEnvironment: 'node',
  testMatch: [
    '**/*.spec.[jt]s?(x)',
  ],
  setupFiles: [
    '<rootDir>/test/unit/setup',
  ],
  coverageDirectory: 'test/unit/coverage',
  collectCoverageFrom: [
    'src/**/*.{js}',
    '!src/**/*.spec.{js}',
    '!src/index.js',
    '!src/schema.js',
    '!**/node_modules/**',
    '!**/test/**',
  ],
  coverageReporters: ['json', 'lcov', 'text', 'html'],
};
